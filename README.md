# Tutoriels LP CISIIE

## module html avancé

Construire un tutoriel basé sur une démo sur un des thèmes listés ci-dessous.
Chaque tutoriel correspond à un des répertoires du projet.
Le tutoriel en lui-même, ou un lien, doit être décrit dans le fichier README.md du répertoire, en utilisant la syntaxe markdown.

## DATE DE RENDU : 13 novembre 2016, 24:00

## thèmes prévus : 
1. css transition, multiple column layout: lebrun, labroche, wurth
2. css gradients, css color module : collin, dupuis, giovannelli
3. filter effects, blending mode: lambiase, weiten, claudel, demougin
4. css animation : chopin, antoine, kartner
5. css transform : remaki, el ouarchi
6. typographie, @font-face & webfonts, fluid typo : Larrière, becker, torzuoli, reeb
7. responsive images : launay, pierre, postel, pereira
8. grid, grid vs flex : weber, barré, ruhlmann, latève-houssemand

## Ce qui est attendu : 
1. introduction, rappels et pointeur vers la spécification W3C et une référence de qualité et complète
2. le tutoriel expliquant l'intéret et l'utilisation du module css, basé sur une démo
3. la démo elle-même, avec les sources
4. un point sur l'implantation dans les navigateurs
5. une liste de ressources/exemples/tutoriels existant sur le même thème


## Utilisation du dépôt

**Ne travaillez pas directement sur ce dépôt**

Faites un fork pour créer un dépôt dont vous êtes propriétaire.

Utilisez ce nouveau dépôt : clonez-le sur votre machine. 

**Créez une branche portant le nom de votre tuto !**

Faites vos commits/push sur votre dépôt. Quand votre tutoriel est prêt, faites un pull request pour l'intégrer dans ce dépôt.

